
void DHT_init(){
  DHT.setup(DHT_PIN, DHTesp::DHT11);
  Serial.println("DHT ready !");
}

float getTempIndex(){
  delay(DHT.getMinimumSamplingPeriod());
  float humidity = DHT.getHumidity();
  float temperature = DHT.getTemperature();
  return DHT.computeHeatIndex(temperature, humidity, false);
}
