extern "C" {
  #include <user_interface.h>
}
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <DHTesp.h>
#include <ESP8266mDNS.h>
#include <FS.h>


IPAddress apIP(192, 168, 4, 1);
ESP8266WebServer HTTP(80);
String ssidAP     = "projZesp";
String passwordAP = "01234567";

DHTesp DHT;
#define DHT_PIN 4
#define PR_PIN A0


void setup() {
  Serial.begin(230400);
  Serial.println("");
  WIFI_init();
  DHT_init();
  FS_init();
  HTTP_init();
}

void loop() {
  HTTP.handleClient();
  delay(1);
}
