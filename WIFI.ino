bool WIFI_init(void) {
  Serial.println("WIFI initalizing...");
  WiFi.disconnect();
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssidAP.c_str(), passwordAP.c_str());
  Serial.println("WiFi access point created");
  Serial.print("Name: ");
  Serial.println(ssidAP.c_str());
  Serial.print("Password: ");
  Serial.println(passwordAP.c_str());
  Serial.print("IP address: ");
  Serial.println(apIP);
  return true;
}
