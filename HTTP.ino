bool HTTP_init(void) {
  HTTP.on("/", handleMain);
  HTTP.on("/values", handleJsonValues);
  HTTP.on("/jquery-3.3.1.min.js", handleJQuery);
  HTTP.on("/restart", handleRestart);
  HTTP.onNotFound(handleMain);
  HTTP.begin();
  Serial.println("Web server started.");
  return true;
}


void handleMain() {
  handleFileRead("/");
}

void handleJsonValues() {
  String json = "{";
  json += "\"light\":\"";
  json += String(getLightLevel());
  json += "\",\"temp\":\"";
  json += String(DHT.getTemperature());
  json += "\",\"hum\":\"";
  json += String(DHT.getHumidity());
  json += "\"}";
  HTTP.send(200, "text/json", json);
}

void handleJQuery() {
    handleFileRead("/jquery-3.3.1.min.js");
}

//http://192.168.4.1/restart?device=ok
void handleRestart() {
  String restart = HTTP.arg("device");
  if (restart == "ok") {
    HTTP.send(200, "text / plain", "Reset OK");
    ESP.restart();
  }
  else {
    HTTP.send(500, "text / plain", "Internal Server Error ");
  }
}
